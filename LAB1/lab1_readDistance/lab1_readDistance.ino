//LAB 1, Measure distance with HC-SR04 sensor

const int EchoPin = 12;
const int TriggerPin = 13;
 
void setup() {
   Serial.begin(9600);
   pinMode(TriggerPin, OUTPUT);
   pinMode(EchoPin, INPUT);
}
 
void loop() {
   int cm = measureDist(TriggerPin, EchoPin);
   Serial.print("Distancia: ");
   Serial.println(cm);
   delay(1000);
}
 
int measureDist(int TriggerPin, int EchoPin) {
   long duration, distanceCm;

   //Force the pin to have a low signal to make sure
   //it registers a high for 10 microseconds so that
   //the reading is initiated. 
   digitalWrite(TriggerPin, LOW);   
   delayMicroseconds(5);
   //Initiate sensor with pulse
   digitalWrite(TriggerPin, HIGH);
   delayMicroseconds(10);
   digitalWrite(TriggerPin, LOW); 

   //Measure the time the sound travelled measuring
   //the width of the pulse in pin "Echo"
   duration = pulseIn(EchoPin, HIGH);
   //Conversion from time to distance (in cm). Value
   //obtained from the sensor's data-sheet. 
   distanceCm = duration/58;
   return distanceCm;
}
