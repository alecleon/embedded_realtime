#include <ncurses.h>
#include <stdlib.h>
#include <menu.h>
#include "life.h"

#define TIME_OUT  300

int STARTX = 0;
int STARTY = 0;
int ENDX = 0;
int ENDY = 0;

struct state;

int main()
{

//Initialization
	initscr();
	cbreak();
	timeout(TIME_OUT);
	keypad(stdscr, TRUE);
	int i, j;

	ENDX = COLS - 1;
	ENDY = LINES - 1;

//Memory management: allocating
	state **workarea = (state **)malloc(COLS*sizeof(state *));
	for(i = 0;i < COLS; ++i)
		workarea[i] = (state *)malloc(LINES*sizeof(state));

	init_figure(workarea); //Display menu to choose the initial workarea
	update_state(workarea, STARTX, STARTY, ENDX, ENDY); //Update old states

//GAME OF LIFE STARTS
	while(getch() != 32)
	{
		for(i = STARTX; i <= ENDX; ++i)
			for(j = STARTY; j <= ENDY; ++j)
				neighbours(workarea, i, j, COLS, LINES); //Get neighbours and GoL rules
		update_state(workarea, STARTX, STARTY, ENDX, ENDY);//Update old states
		display(stdscr,  workarea, STARTX, STARTY, ENDX, ENDY);//Display new states
	}
//GAME OVER

	//Memory management: deallocating
	for(i = 0;i < COLS; ++i)
		free(workarea[i]);
	free(workarea);
	endwin();
	return 0;
}
