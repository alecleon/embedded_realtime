#include <curses.h>
#include <stdlib.h>
#include "life.h"


#define CELL_CHAR '#'


void display(WINDOW *win, state **area, int startx, int starty, int endx, int endy)
{
  int i, j;
	wclear(win);
  mvprintw(0, 0,"-------------------- GAME OF LIFE --------------------");
  mvprintw(LINES-1, 0,"(Press spacebar to exit)");
	for(i = startx; i <= endx; ++i)
		for(j = starty;j <= endy; ++j)
			if(area[i][j].newstate == TRUE)
				mvwaddch(win, j, i, CELL_CHAR);

	wrefresh(win);
}

void neighbours(state **area, int i, int j, int cols, int lines)
{	int neighbours = 0;
	bool newstate;
  int x;
  int y;

  //explores 9 cells arround i,j -including itself-
  for(x = -1; x<=1; x++)
  {
    for(y = -1; y<=1; y++)
    {
      if ((i+x < 0) || (j+y < 0) || (i+x > (cols-1)) || (j+y > (lines-1)));
      else
      neighbours += area[i+x][j+y].oldstate;
    }

  }
  neighbours -= area[i][j].oldstate; //To not count itself

//Aply rules
	newstate = FALSE;
	if(area[i][j].oldstate == TRUE && (neighbours == 2 || neighbours == 3))
		 newstate = TRUE;
	else
		if(area[i][j].oldstate == FALSE && neighbours == 3)
			 newstate = TRUE;
	area[i][j].newstate = newstate;
}

void update_state(state **area, int startx, int starty, int endx, int endy)
{
	int i, j;

	for(i = startx; i <= endx; ++i)
		for(j = starty; j <= endy; ++j)
			area[i][j].oldstate = area[i][j].newstate;
}

void init_figure(state **area)
{

  switch (menu_display()) {
    case 0:
        // Block
        area[37][13].newstate = TRUE;
        area[37][14].newstate = TRUE;
        area[38][13].newstate = TRUE;
        area[38][14].newstate = TRUE;
        break;

    case 1:
        // Beehive
        area[36][14].newstate = TRUE;
        area[37][13].newstate = TRUE;
        area[37][15].newstate = TRUE;
        area[38][13].newstate = TRUE;
        area[38][15].newstate = TRUE;
        area[39][14].newstate = TRUE;
        break;

    case 2:
        // Loaf
        area[36][14].newstate = TRUE;
        area[37][13].newstate = TRUE;
        area[37][15].newstate = TRUE;
        area[38][13].newstate = TRUE;
        area[38][16].newstate = TRUE;
        area[39][14].newstate = TRUE;
        area[39][15].newstate = TRUE;
        break;

    case 3:
        // Boat
        area[37][13].newstate = TRUE;
        area[37][14].newstate = TRUE;
        area[38][13].newstate = TRUE;
        area[38][15].newstate = TRUE;
        area[39][14].newstate = TRUE;
        break;

    case 4:
        // Glider
        area[36][15].newstate = TRUE;
        area[37][13].newstate = TRUE;
        area[37][15].newstate = TRUE;
        area[38][14].newstate = TRUE;
        area[38][15].newstate = TRUE;
        break;

    case 5:
        // Lightweight spaceship (LWSS)
        area[36][13].newstate = TRUE;
        area[36][15].newstate = TRUE;
        area[37][16].newstate = TRUE;
        area[38][16].newstate = TRUE;
        area[39][13].newstate = TRUE;
        area[39][16].newstate = TRUE;
        area[40][14].newstate = TRUE;
        area[40][15].newstate = TRUE;
        area[40][16].newstate = TRUE;
        break;

    case 6:
        // Horizontal line (1x3)
        area[37][13].newstate = TRUE;
        area[38][13].newstate = TRUE;
        area[39][13].newstate = TRUE;
        break;

    case 7:
        // Toad (period 2)
        area[36][14].newstate = TRUE;
        area[37][13].newstate = TRUE;
        area[37][14].newstate = TRUE;
        area[38][13].newstate = TRUE;
        area[38][14].newstate = TRUE;
        area[39][13].newstate = TRUE;
        break;

    case 8:
        // Beacon (period 2)
        area[36][13].newstate = TRUE;
        area[36][14].newstate = TRUE;
        area[37][13].newstate = TRUE;
        area[38][16].newstate = TRUE;
        area[39][15].newstate = TRUE;
        area[39][16].newstate = TRUE;
        break;

    case 9:
        // The R-pentomino
        area[36][14].newstate = TRUE;
        area[37][13].newstate = TRUE;
        area[37][14].newstate = TRUE;
        area[37][15].newstate = TRUE;
        area[38][13].newstate = TRUE;
        break;

    case 10:
        // Diehard
        area[37][13].newstate = TRUE;
        area[38][13].newstate = TRUE;
        area[38][14].newstate = TRUE;
        area[41][14].newstate = TRUE;
        area[42][12].newstate = TRUE;
        area[42][14].newstate = TRUE;
        area[43][14].newstate = TRUE;
        break;

    case 11:
        // Acorn
        area[36][15].newstate = TRUE;
        area[37][13].newstate = TRUE;
        area[37][15].newstate = TRUE;
        area[39][14].newstate = TRUE;
        area[40][15].newstate = TRUE;
        area[41][15].newstate = TRUE;
        area[42][15].newstate = TRUE;
        break;

    default:
        // L
        area[37][13].newstate = TRUE;
        area[37][14].newstate = TRUE;
        area[38][14].newstate = TRUE;
        break;

  }// switch()



}// init_figure
