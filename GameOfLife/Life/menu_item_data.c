#include <ncurses.h>
#include <stdlib.h>
#include <menu.h>
#include "life.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
#define CTRLD 	4

char *init_figures[] = {
                        "Block",
                        "Beehive",
                        "Loaf",
                        "Boat",
                        "Glider",
                        "Lightweight spaceship (LWSS)",
                        "Horizontal line (1x3)",
                        "Toad (period 2)",
                        "Beacon (period 2)",
                        "The R-pentomino",
                        "Diehard",
                        "Acorn"
                  };
char *description[] = { };

int menu_display()
{
  ITEM **my_items;
	int c;
	MENU *menu;
  int n_choices, i;
	ITEM *cur_item;


	initscr();
        cbreak();
        noecho();
	keypad(stdscr, TRUE);

        n_choices = ARRAY_SIZE(init_figures);
        my_items = (ITEM **)malloc((n_choices + 1)*sizeof(ITEM *));

        for(i = 0; i < n_choices; ++i)
                my_items[i] = new_item(init_figures[i], description[i]);
	my_items[n_choices] = (ITEM *)NULL;

	menu = new_menu((ITEM **)my_items);
	post_menu(menu);
	refresh();

	while((c = getch()) != 8)
	{       switch(c)
	        {	case KEY_DOWN:
      				menu_driver(menu, REQ_DOWN_ITEM);
      				break;
      			case KEY_UP:
      				menu_driver(menu, REQ_UP_ITEM);
      				break;
      			case 10:	/* Enter */
      				cur_item = current_item(menu);
      				move(LINES - 2, 0);
      				clrtoeol();

              return item_index(cur_item);
      				refresh();
      				pos_menu_cursor(menu);
      				break;
          }

	}


	free_item(my_items[0]);
        free_item(my_items[1]);
	free_menu(menu);
  free(my_items);
	endwin();
}
