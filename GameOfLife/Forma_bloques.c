// Definicion de los bloques a inciar

//-------- Still lifes --------//
// Block
workarea[37][13].newstate = TRUE;
workarea[37][14].newstate = TRUE;
workarea[38][13].newstate = TRUE;
workarea[38][14].newstate = TRUE;

// Beehive
workarea[36][14].newstate = TRUE;
workarea[37][13].newstate = TRUE;
workarea[37][15].newstate = TRUE;
workarea[38][13].newstate = TRUE;
workarea[38][15].newstate = TRUE;
workarea[39][14].newstate = TRUE;

// Loaf
workarea[36][14].newstate = TRUE;
workarea[37][13].newstate = TRUE;
workarea[37][15].newstate = TRUE;
workarea[38][13].newstate = TRUE;
workarea[38][16].newstate = TRUE;
workarea[39][14].newstate = TRUE;
workarea[39][15].newstate = TRUE;

// Boat
workarea[37][13].newstate = TRUE;
workarea[37][14].newstate = TRUE;
workarea[38][13].newstate = TRUE;
workarea[38][15].newstate = TRUE;
workarea[39][14].newstate = TRUE;

//--------- Spaceships ---------//
// Glider
workarea[36][15].newstate = TRUE;
workarea[37][13].newstate = TRUE;
workarea[37][15].newstate = TRUE;
workarea[38][14].newstate = TRUE;
workarea[38][15].newstate = TRUE;

// Lightweight spaceship (LWSS)
workarea[36][13].newstate = TRUE;
workarea[36][15].newstate = TRUE;
workarea[37][16].newstate = TRUE;
workarea[38][16].newstate = TRUE;
workarea[39][13].newstate = TRUE;
workarea[39][16].newstate = TRUE;
workarea[40][14].newstate = TRUE;
workarea[40][15].newstate = TRUE;
workarea[40][16].newstate = TRUE;

//-------- Oscillators --------//
// Horizontal line (1x3)
workarea[37][13].newstate = TRUE;
workarea[38][13].newstate = TRUE;
workarea[39][13].newstate = TRUE;

// Toad (period 2)
workarea[36][14].newstate = TRUE;
workarea[37][13].newstate = TRUE;
workarea[37][14].newstate = TRUE;
workarea[38][13].newstate = TRUE;
workarea[38][14].newstate = TRUE;
workarea[39][13].newstate = TRUE;

// Beacon (period 2)
workarea[36][13].newstate = TRUE;
workarea[36][14].newstate = TRUE;
workarea[37][13].newstate = TRUE;
workarea[38][16].newstate = TRUE;
workarea[39][15].newstate = TRUE;
workarea[39][16].newstate = TRUE;


//-------------------------------//
// Obligatorias a simular
// The R-pentomino
workarea[36][14].newstate = TRUE;
workarea[37][13].newstate = TRUE;
workarea[37][14].newstate = TRUE;
workarea[37][15].newstate = TRUE;
workarea[38][13].newstate = TRUE;

// Diehard
workarea[37][13].newstate = TRUE;
workarea[38][13].newstate = TRUE;
workarea[38][14].newstate = TRUE;
workarea[41][14].newstate = TRUE;
workarea[42][12].newstate = TRUE;
workarea[42][14].newstate = TRUE;
workarea[43][14].newstate = TRUE;

// Acorn
workarea[36][15].newstate = TRUE;
workarea[37][13].newstate = TRUE;
workarea[37][15].newstate = TRUE;
workarea[39][14].newstate = TRUE;
workarea[40][15].newstate = TRUE;
workarea[41][15].newstate = TRUE;
workarea[42][15].newstate = TRUE;
