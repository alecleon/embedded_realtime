%% This code reads the temperature from the LM35 sensor connected to the Arduino
clear all
UNO = arduino();

while (1)
    
    voltage = readVoltage(UNO, 'A0');
    temp = voltage*100;
    disp(temp);
    
end